//=========================================
// Helper Functions
//=========================================

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    const winningLine = lines.find(([a,b,c]) => {
        return squares[a] && squares[a] === squares[b] && squares[a] === squares[c];
    });

    if (winningLine) {
        return squares[winningLine[0]];
    } else {
        return null;
    }
}

//=========================================
// React Component Definitions
//=========================================

function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
} 

function Row(props) {
    return (
        <div className="board-row">
            {
                [0,1,2].map(col => {
                    const i = props.row * 3 + col;
                    return <Square
                               key={"square-" + i}
                               value={props.squares[i]}
                               onClick={() => props.onClick(i)}
                           />;
                })
            }
        </div>
    );
}

function Board(props) {
    return (
        <div>
            {
                [0,1,2].map(row => (
                        <Row
                            key={"row-" + row}
                            row={row}
                            squares={props.squares}
                            onClick={props.onClick}
                        />
                ))
            }
        </div>
    );
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.jumpTo = this.jumpTo.bind(this);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (!squares[i] && !calculateWinner(squares)) {
            squares[i] = this.state.xIsNext ? 'X' : 'O';
            this.setState({
                history: history.concat([{
                    squares: squares,
                }]),
                stepNumber: history.length,
                xIsNext: !this.state.xIsNext,
            });
        }
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move ?
                  'Go to move #' + move :
                  'Go to game start';
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        const status = winner ?
              'Winner: ' + winner :
              'Next player: ' + (this.state.xIsNext ? 'X' : 'O');

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={this.handleClick}
                    />
                </div>
                <div className="game-info">
                    <div>{ status }</div>
                    <ol>{ moves }</ol>
                </div>
            </div>
        );
    }
}

//=========================================
// Render Root Component
//=========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
