var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//=========================================
// Helper Functions
//=========================================

function calculateWinner(squares) {
    var lines = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

    var winningLine = lines.find(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 3),
            a = _ref2[0],
            b = _ref2[1],
            c = _ref2[2];

        return squares[a] && squares[a] === squares[b] && squares[a] === squares[c];
    });

    if (winningLine) {
        return squares[winningLine[0]];
    } else {
        return null;
    }
}

//=========================================
// React Component Definitions
//=========================================

function Square(props) {
    return React.createElement(
        "button",
        { className: "square", onClick: props.onClick },
        props.value
    );
}

function Row(props) {
    return React.createElement(
        "div",
        { className: "board-row" },
        [0, 1, 2].map(function (col) {
            var i = props.row * 3 + col;
            return React.createElement(Square, {
                key: "square-" + i,
                value: props.squares[i],
                onClick: function onClick() {
                    return props.onClick(i);
                }
            });
        })
    );
}

function Board(props) {
    return React.createElement(
        "div",
        null,
        [0, 1, 2].map(function (row) {
            return React.createElement(Row, {
                key: "row-" + row,
                row: row,
                squares: props.squares,
                onClick: props.onClick
            });
        })
    );
}

var Game = function (_React$Component) {
    _inherits(Game, _React$Component);

    function Game(props) {
        _classCallCheck(this, Game);

        var _this = _possibleConstructorReturn(this, (Game.__proto__ || Object.getPrototypeOf(Game)).call(this, props));

        _this.handleClick = _this.handleClick.bind(_this);
        _this.jumpTo = _this.jumpTo.bind(_this);
        _this.state = {
            history: [{
                squares: Array(9).fill(null)
            }],
            stepNumber: 0,
            xIsNext: true
        };
        return _this;
    }

    _createClass(Game, [{
        key: "handleClick",
        value: function handleClick(i) {
            var history = this.state.history.slice(0, this.state.stepNumber + 1);
            var current = history[history.length - 1];
            var squares = current.squares.slice();
            if (!squares[i] && !calculateWinner(squares)) {
                squares[i] = this.state.xIsNext ? 'X' : 'O';
                this.setState({
                    history: history.concat([{
                        squares: squares
                    }]),
                    stepNumber: history.length,
                    xIsNext: !this.state.xIsNext
                });
            }
        }
    }, {
        key: "jumpTo",
        value: function jumpTo(step) {
            this.setState({
                stepNumber: step,
                xIsNext: step % 2 === 0
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            var history = this.state.history;
            var current = history[this.state.stepNumber];
            var winner = calculateWinner(current.squares);

            var moves = history.map(function (step, move) {
                var desc = move ? 'Go to move #' + move : 'Go to game start';
                return React.createElement(
                    "li",
                    { key: move },
                    React.createElement(
                        "button",
                        { onClick: function onClick() {
                                return _this2.jumpTo(move);
                            } },
                        desc
                    )
                );
            });

            var status = winner ? 'Winner: ' + winner : 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');

            return React.createElement(
                "div",
                { className: "game" },
                React.createElement(
                    "div",
                    { className: "game-board" },
                    React.createElement(Board, {
                        squares: current.squares,
                        onClick: this.handleClick
                    })
                ),
                React.createElement(
                    "div",
                    { className: "game-info" },
                    React.createElement(
                        "div",
                        null,
                        status
                    ),
                    React.createElement(
                        "ol",
                        null,
                        moves
                    )
                )
            );
        }
    }]);

    return Game;
}(React.Component);

//=========================================
// Render Root Component
//=========================================

ReactDOM.render(React.createElement(Game, null), document.getElementById('root'));