;;=========================================
;; Helper Functions
;;=========================================

(defn calculate-winner [squares]
  (let [lines [[0 1 2]
               [3 4 5]
               [6 7 8]
               [0 3 6]
               [1 4 7]
               [2 5 8]
               [0 4 8]
               [2 4 6]]]
    (some (fn [[a b c]]
            (and (= (squares a) (squares b) (squares c))
                 (squares a)))
          lines)))

;;=========================================
;; React Component Definitions
;;=========================================

(defn square [props]
  [:button.square
   {:on-click (props :on-click)}
   (props :value)])

(defn row [props]
  [:div.board-row
   (map (fn [col]
          (let [i (+ (* (props :row) 3) col)]
            [square {:key      (str "square-" i)
                     :value    (get-in props [:squares i])
                     :on-click #((props :on-click) i)}]))
        [0 1 2])])

(defn board [props]
  [:div
   (map (fn [row]
          [row {:key      (str "row-" row)
                :row      row
                :squares  (props :squares)
                :on-click (props :on-click)}])
        [0 1 2])])

(def state (reagent/atom {:history     [{:squares (into [] (repeat 9 null))}]
                          :step-number 0
                          :x-is-next   true}))

(defn handle-click [i]
  (let [history (subvec (@state :history) 0 (inc (@state :step-number)))
        current (peek history)
        squares (current :squares)]
    (if-not (or (squares i) (calculate-winner squares))
      (let [player (if (@state :x-is-next) "X" "O")]
        (swap! state assoc
               :history     (conj history {:squares (assoc squares i player)})
               :step-number (count history)
               :x-is-next   (not (@state :x-is-next)))))))

(defn jump-to [step]
  (swap! state assoc
         :step-number step
         :x-is-next   (even? step)))

(defn game []
  (let [history (@state :history)
        current (history (@state :step-number))
        winner  (calculate-winner (current :squares))
        moves   (map-indexed
                 (fn [move step]
                   (let [desc (if (zero? move)
                                "Go to game start"
                                (str "Go to move #" move))]
                     [:li {:key move}
                      [:button {:on-click #(jump-to move)}
                       desc]]))
                 history)
        status  (if winner
                  (str "Winner: " winner)
                  (str "Next player: " (if (@state :x-is-next) "X" "O")))]
    [:div.game
     [:div.game-board
      [board {:squares (current :squares)
              :on-click handleClick}]]
     [:div.game-info
      [:div status]
      [:ol moves]]]))

;;=========================================
;; Render Root Component
;;=========================================

(reagent/render
 [game]
 (js/document.getElementById "root"))
